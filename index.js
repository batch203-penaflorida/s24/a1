let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = ["258 Washington Ave NW", "Californa 90011"];

const [street, state] = address;

console.log(`I live at ${street}, ${state}`);

const animal = {
  name: "Lolong",
  breed: "saltwater crocodile",
  weight: "1075",
  length: "20 ft 3 in",
};
const { name, breed, weight, length } = animal;

console.log(
  `${name} was a ${breed}. He weight at ${weight} kgs with a measurement of ${length}.`
);

const arrOfNumbers = [1, 2, 3, 4, 5];

arrOfNumbers.forEach((number) => console.log(`${number}`));

const reduceNumber = [1, 2, 3, 4, 5];
const initialValue = 0;
const sumTotalOfReduce = reduceNumber.reduce(
  (acc, cur) => acc + cur,
  initialValue
);

console.log(sumTotalOfReduce);
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);
